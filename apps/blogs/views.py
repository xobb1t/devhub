from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import redirect, get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, CreateView
from django.views.decorators.http import require_POST

from .favorites import get_posts_in_feed, follow, unfollow
from .forms import PostForm
from .models import Post, Blog


class LoginRequiredMixin(object):

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class BlogMixin(LoginRequiredMixin):

    model = Post

    def get_blog(self):
        if 'blog_pk' in self.kwargs:
            return get_object_or_404(Blog, pk=self.kwargs.get('blog_pk'))
        return self.request.user.blog

    def get(self, request, *args, **kwargs):
        self.blog = self.get_blog()
        return super(BlogMixin, self).get(request, *args, **kwargs)

    def get_queryset(self):
        return Post.objects.in_blog(self.blog)

    def get_context_data(self, **kwargs):
        kwargs.update({'blog': self.blog})
        return super(BlogMixin, self).get_context_data(**kwargs)


class PostListView(BlogMixin, ListView):

    pass


class PostDetailView(BlogMixin, DetailView):

    pass


class FeedView(LoginRequiredMixin, ListView):

    template_name = 'blogs/feed.html'

    def get_queryset(self):
        return get_posts_in_feed(self.request.user)


class PostCreateView(BlogMixin, CreateView):

    form_class = PostForm
    model = Post

    def get_blog(self):
        return self.request.user.blog

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.blog = self.blog
        self.object.save()
        return redirect(self.get_success_url())


@login_required
@require_POST
def mark_post_watched(self):
    post_pk = self.request.POST.get('post_pk')
    post = get_object_or_404(Post, pk=post_pk)
    post.mark_watched(self.request.user)
    return redirect('blog:feed')


@login_required
@require_POST
def follow(self, blog_pk):
    blog = get_object_or_404(Blog, pk=post_pk)
    follow(user, blog)
    return redirect(blog.get_absolute_url())


@login_required
@require_POST
def unfollow(self, blog_pk):
    blog = get_object_or_404(Blog, pk=post_pk)
    unfollow(user, blog)
    return redirect(blog.get_absolute_url())
