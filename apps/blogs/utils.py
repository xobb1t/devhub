from django.db.models import Q
from .models import Post, FavoriteUsers


def get_favorite_data(user):
    favorite_data, _ = FavoriteUsers.objects.get_or_create(
        owner=user
    )
    return favorite_data


def get_posts_in_feed(user):
    favorite_data = get_favorite_data(user)
    favorite_authors_ids = favorite_data.favorites.values_list('pk')
    posts_in_feed = Post.objects.filter(
        Q(author=user) | Q(author__id__in=favorite_authors_ids)
    )
    return posts_in_feed


def follow_user(owner, target_user):
    favorite_data = get_favorite_data(owner)
    favorite_data.favorites.add(target_user)


def unfollow_user(owner, target_user):
    favorite_data = get_favorite_data(owner)
    favorite_data.favorites.remove(target_user)
