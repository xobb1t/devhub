# coding: utf-8
from django.conf import settings
from django.core.mail import get_connection, EmailMessage
from django.template.loader import render_to_string


def send_notifications(post):
    blog = post.blog
    followers = blog.followers.exclude(Q(email='') | Q(email=None))
    emails = followers.values_list('email', flat=True)

    subject = render_to_string('blogs/notifications/subject.txt', {
        'blog': blog, 'post': post,
    })
    body = render_to_string('blogs/notifications/body.txt', {
        'blog': blog, 'post': post,
    })
    # Данный спопоб позволит отправить в рамках одного соединения сообщения всем подписчикам
    # При этом в поле "кому" будет отображен только 1 адрес - адрес подписчика.
    messages = []
    for email in emails:
        messages.append(EmailMessage(
            subject=''.join(subject.splitlines()),
            body=body,
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[email],
        ))
    email_connection = get_connection(fail_silently=False)
    email_connection.send_messages(messages)
