from django.conf.urls import url, patterns
from . import views


urlpatterns = patterns(
    '',
    url(r'^$', views.FeedView.as_view(), name='feed'),
    url(r'^my/$', views.PostListView.as_view(), name='post_list'),
    url(r'^my/create/$', views.PostCreateView.as_view(), name='post_create'),
    url(r'^(?P<blog_pk>\d+)/$', views.PostListView.as_view(),
        name='post_list'),
    url(r'^(?P<blog_pk>\d+)/follow/$', views.follow, name='follow'),
    url(r'^(?P<blog_pk>\d+)/unfollow/$', views.unfollow, name='unfollow'),
    url(r'^(?P<blog>\d+)/(?P<pk>\d+)/$', views.PostDetailView.as_view(),
        name='post_detail'),
    url(r'^(?P<blog>\d+)/(?P<pk>\d+)/watch/$', views.mark_post_watched,
        name='mark_post_watched'),
)
