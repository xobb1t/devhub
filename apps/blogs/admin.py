from django.contrib import admin
from .models import Blog, Post


class PostAdmin(admin.ModelAdmin):

    list_display = ['blog', 'title', 'created_at']
    list_filter = ['created_at', 'blog']


admin.site.register(Blog)
admin.site.register(Post, PostAdmin)
