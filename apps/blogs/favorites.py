from django.db.models import Q
from .models import Post


def get_posts_in_feed(user):
    favorite_blogs = users.favorite_blogs.all()
    posts_in_feed = Post.objects.filter(
        Q(blog=user.blog) | Q(blog__in=favorite_blogs)
    )
    return posts_in_feed


def follow(user, blog):
    blog.followers.add(user)


def unfollow(user, blog):
    blog.followers.remove(user)
