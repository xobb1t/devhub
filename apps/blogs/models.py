# coding: utf-8

from datetime import datetime

from django.conf import settings
from django.db import models
from django.db.models import Q
from django.dispatch import receiver

from annoying.fields import AutoOneToOneField

from .notifications import send_notifications


User = settings.AUTH_USER_MODEL


class Blog(models.Model):

    user = AutoOneToOneField(User, related_name='blog')
    followers = models.ManyToManyField(User, related_name='favorite_blogs')

    def __unicode__(self):
        return u'Блог пользователя {}'.format(self.user)

    @models.permalink
    def get_absolute_url(self):
        return ('blog:post_list', (), {'blog_pk': self.pk})


class PostQuerySet(models.query.QuerySet):

    def in_blog(self, blog):
        return self.filter(blog=blog)


class Post(models.Model):

    blog = models.ForeignKey(Blog, related_name='posts')

    title = models.CharField(u'заголовок', max_length=255)
    text = models.TextField(u'текст')
    created_at = models.DateTimeField(default=datetime.now)
    watched_by = models.ManyToManyField(User, related_name='watched_posts')

    objects = PostQuerySet.as_manager()

    class Meta:
        ordering = ['-created_at']

    def __unicode__(self):
        return self.title

    def is_watched(self, user):
        return self.watched_by.filter(pk=user.pk).exists()

    def mark_watched(self, user):
        watch_obj = self.watched_by.add(user)

    @models.permalink
    def get_absolute_url(self):
        return ('blog:post_detail', (), {'blog_pk': self.blog_id, 'pk': self.pk})


@receiver(models.signals.post_save, sender=Post)
def notify_blog_followers(sender, instance, **kwargs):
    if not kwargs.get('created') or kwargs.get('raw', False):
        return
    send_notifications(instance)
